#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Programa cliente que abre un socket a un servidor."""

import socket
import sys

# Cliente UDP simple.

# Dirección IP del servidor.
try:
    metodo = sys.argv[1]
    mensaje = sys.argv[2]
    IP = mensaje.split('@')[1].split(':')[0]
    PUERTO = mensaje.split('@')[1].split(':')[1]
except ValueError:
    sys.exit("Usage: python3 client.py method reciver@IP:SIPport")
except IndexError:
    sys.exit("Usage: python3 client.py method reciver@IP:SIPport")
# Contenido que vamos a enviar

LINE = (metodo + " sip:" + mensaje + " SIP/2.0")

# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    my_socket.connect((IP, int(PUERTO)))

    print("Enviando datos...: " + LINE)
    my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
    data = my_socket.recv(1024)

    print('Recibido -- ', data.decode('utf-8'))

    if 'SIP/2.0 400 Bad Request' in data.decode('utf-8'):
        sys.exit()

    if metodo == 'INVITE':  # Unicamente enviamos ACK si hemos recibido INVITE
        respuestaser = data.decode('utf-8')
        respuestaser2 = respuestaser.split("\r\n")[4]
        if respuestaser2 == 'SIP/2.0 200 OK':  # Si respuesta OK enviamos ACK
            LINE2 = ('ACK' + ' sip:' + mensaje + " SIP/2.0")
            print("Enviando ACK...: " + LINE2)
            my_socket.send(bytes(LINE2, 'utf-8') + b'\r\n')

    print("Terminando socket...")
    print("Fin.")
