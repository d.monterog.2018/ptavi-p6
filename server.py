#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Clase (y programa principal) para un servidor de eco en UDP simple."""

import socketserver
import sys
import simplertp
import secrets


class EchoHandler(socketserver.DatagramRequestHandler):
    """Echo server class."""

    def handle(self):
        """Echo handle class."""
        # Escribe dirección y puerto del cliente (de tupla client_address)

        for line in self.rfile:
            mensaje = line.decode('utf-8')
            print(mensaje)
            if mensaje != '\r\n':
                metodo = mensaje.split(" ")[0]
                correo = mensaje.split(" ")[1].split(":")[1]
                puerto = mensaje.split(" ")[1].split(":")[2]
                sip = mensaje.split(" ")[1].split(":")[0]

                if sip != 'sip':
                    respuesta = (b"SIP/2.0 400 Bad Request\r\n\r\n")
                    self.wfile.write(respuesta)

                elif metodo == 'INVITE':
                    respuesta = (b"SIP/2.0 100 Trying\r\n\r\n")
                    respuesta2 = (respuesta + b"SIP/2.0 180 Ringing\r\n\r\n")
                    respuesta3 = (respuesta2 + b"SIP/2.0 200 OK\r\n")
                    respuesta4 = (respuesta3 +
                                  b"Content-Type: application/sdp\r\n\r\n"
                                  b"v=0\r\no=" + bytes(correo, 'utf-8')
                                  + b"\r\ns=normal\r\nt=0\r\nm=audio " +
                                  bytes(puerto, 'utf-8') + b" RTP\r\n")
                    self.wfile.write(respuesta4)

                elif metodo == 'BYE':
                    respuesta = (b"SIP/2.0 200 OK\r\n\r\n")
                    self.wfile.write(respuesta)

                elif metodo == 'ACK':
                    BIT = secrets.randbelow(1)
                    RTP_header = simplertp.RtpHeader()
                    RTP_header.set_header(version=2, marker=BIT,
                                          payload_type=14, ssrc=200002)
                    audio = simplertp.RtpPayloadMp3(pista)
                    simplertp.send_rtp_packet(RTP_header, audio, ip, 23032)

                elif metodo != ('INVITE', 'BYE', 'ACK'):
                    respuesta = (b"SIP/2.0 405 Method Not Allowed\r\n\r\n")
                    self.wfile.write(respuesta)


if __name__ == "__main__":
    # Creamos servidor de eco y escuchamos
    try:
        puerto = int(sys.argv[2])
        ip = sys.argv[1]
        pista = sys.argv[3]
    except IndexError:
        sys.exit("Usage: python3 server.py IP port audio_file")
    except ValueError:
        sys.exit("Usage: python3 server.py IP port audio_file")

    serv = socketserver.UDPServer((ip, puerto), EchoHandler)
    print("Lanzando servidor UDP de eco...")
    print("Listening...")
    serv.serve_forever()
